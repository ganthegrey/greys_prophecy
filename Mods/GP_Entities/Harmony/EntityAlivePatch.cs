﻿using DMT;
using Harmony;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

public class EntityAlivePatch : IHarmony
{
    public void Start()
    {
        var patchName = GetType().ToString();

        Debug.Log(" Loading Patch: " + patchName);
        var harmony = HarmonyInstance.Create(patchName);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    // code curtesy of Sphereii's modlet: https://github.com/SphereII/SphereII.Mods/blob/master/0-SphereIICore/Harmony/ZombieFeatures/RandomWalk.cs
    [HarmonyPatch(typeof(EntityAlive), "CopyPropertiesFromEntityClass")]
    public class EntityAlivePatch_CopyPropertiesFromEntityClass
    {
        public static void Postfix(EntityAlive __instance, ref int ___walkType)
        {
            if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
            {
                return;
            }

            if ((___walkType != 4) && (___walkType != 8) && __instance is EntityZombie)
            {

                // Distribution of Walk Types in an array. Adjust the numbers as you want for distribution. The 9 in the default int[9] indicates how many walk types you've specified.
                int[] numbers = new int[6] { 1, 2, 3, 5, 6, 7 };

                System.Random random = new System.Random();

                // Randomly generates a number between 0 and the maximum number of elements in the numbers.
                int randomNumber = random.Next(0, numbers.Length);

                // return the randomly selected walk type
                ___walkType = numbers[randomNumber];
                //Debug.Log("Random Walk Type: " + ___walkType);
            }
        }
    }
}
