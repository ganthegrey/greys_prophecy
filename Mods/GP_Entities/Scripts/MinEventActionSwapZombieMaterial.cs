﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;

public class MinEventActionSwapZombieMaterial : MinEventActionBase
{
    private string _swapMaterialPropertyName;
    private int _materialIndex;
    private bool _useOriginal;
    
    public override void Execute(MinEventParams _params)
    {
        SkinnedMeshRenderer[] skinnedMeshRenderers = _params.Self.GetComponentsInChildren<SkinnedMeshRenderer>();
        if(skinnedMeshRenderers != null && skinnedMeshRenderers.Any())
        {
            if (_params.Self.EntityName.StartsWith("zombieSoldier") && _materialIndex == 0)
            {
                _materialIndex = 1; // for some reason the soldier's first material is the eyes /facepalm
            }
            foreach(var renderer in skinnedMeshRenderers)
            {
                var materials = renderer.materials;

                if(materials != null && materials.Any())
                {
                    for (var i = 0; i < materials.Length; i++)
                    {
                        if(i == _materialIndex)
                        {
                            var matPath = string.Empty;
                            var entityClass = _params.Self.EntityClass;

                            if (_useOriginal)
                            {
                                if(entityClass.MaterialSwap == null && entityClass.Properties.Contains("OriginalMaterialPath"))
                                {
                                    matPath = entityClass.Properties.Values["OriginalMaterialPath"];
                                }

                                if(entityClass.MaterialSwap != null)
                                {
                                    matPath = entityClass.MaterialSwap[0];
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(_swapMaterialPropertyName) && entityClass.Properties.Contains(_swapMaterialPropertyName))
                                {
                                    matPath = entityClass.Properties.Values[_swapMaterialPropertyName];
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(matPath))
                            {
                                var material = Object.Instantiate(DataLoader.LoadAsset<Material>(matPath));

                                if (material != null)
                                {
                                    materials[i] = material;
                                    renderer.materials = materials;
                                    break;
                                }
                            }

                        }
                    }
                }
            }
        }
        base.Execute(_params);
    }

    public override bool ParseXmlAttribute(XmlAttribute _attribute)
    {
        bool xmlAttribute = base.ParseXmlAttribute(_attribute);
        if (!xmlAttribute)
        {
            var attName = _attribute.Name;

            if (attName.Equals("material_property"))
            {
                _swapMaterialPropertyName = _attribute.Value;
                return true;
            }

            if (attName.Equals("material_index"))
            {
                _materialIndex = int.Parse(_attribute.Value);
                return true;
            }

            if (attName.Equals("use_original"))
            {
                _useOriginal = StringParsers.ParseBool(_attribute.Value);
                return true;
            }
        }

        return xmlAttribute;
    }
}
