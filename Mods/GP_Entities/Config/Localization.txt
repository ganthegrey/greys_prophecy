﻿Key,File,Type,english
npc_traderGan,entityclasses,Entity,Trader Gan
qc_getrekt,quests,Quest - Challenge,"Get Rekt Challenge"
challenge_getrekt,quests,Quest - Challenge,"Get Rekt"
challenge_getrekt_subtitle,quests,Quest - Challenge,"Kill Some Rekt Zombies"
challenge_getrekt_offer,quests,Quest - Challenge,"It's time to exact revenge against that foul-mouthed trader. Word is he's multiplying faster than rabbits. This must be stopped!\n\nChallenge: Kill Some Rekt Zombies"

qc_hughjassproblem,quests,Quest - Challenge,"Hugh Jass Problem Challenge"
challenge_hughjassproblem,quests,Quest - Challenge,"Hugh Jass Problem"
challenge_hughjassproblem_subtitle,quests,Quest - Challenge,"Kill Some Hugh Zombies"
challenge_hughjassproblem_offer,quests,Quest - Challenge,"Like Rekt, Hugh mouthed off to the wrong person and got turned. Do the world a favor and take care of this nonsense!\n\nChallenge: Kill Some Hugh Zombies"
