﻿using DMT;
using Harmony;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

class XUiC_SkillAttributeInfoWindowPatch : IHarmony
{
    public void Start()
    {
        var patchName = GetType().ToString();

        Debug.Log(" Loading Patch: " + patchName);
        var harmony = HarmonyInstance.Create(patchName);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    [HarmonyPatch(typeof(XUiC_SkillAttributeInfoWindow), "Entry_OnHover")]
    private class XUiC_SkillAttributeInfoWindow_Entry_OnHover
    {
        public static bool Prefix(XUiController _sender, OnHoverEventArgs _e)
        {
            var gpController = _sender as XUiC_GPSkillAttributeLevel;

            if (gpController != null && gpController.DisabledFor != null && gpController.DisabledFor.Length > 0)
            {
                if(gpController.xui != null && gpController.xui.selectedSkill != null)
                {
                    return !gpController.DisabledFor.ContainsCaseInsensitive(gpController.xui.selectedSkill.Name);
                }
            }

            return true;
        }
    }
}
