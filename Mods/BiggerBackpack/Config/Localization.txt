﻿Key,Source,Context,Changes,English
perkGPPackMuleRank1LongDesc,progression,Perk,--,"Who has time to organize their stuff perfectly? Carry eight more items without being encumbered."
perkGPPackMuleRank2LongDesc,progression,Perk,--,"You know it's in there somewhere. Carry an additional nine more items without being encumbered."
perkGPPackMuleRank3LongDesc,progression,Perk,--,"You know exactly where everything is. Carry ten more items without being encumbered."
perkGPPackMuleRank4LongDesc,progression,Perk,--,"You just passed inspection private.  Carry twelve more items without being encumbered."
perkGPPackMuleRank5LongDesc,progression,Perk,--,"Now you are just showing off, you are either part mule or a strongman. Carry eighteen more items without being encumbered."