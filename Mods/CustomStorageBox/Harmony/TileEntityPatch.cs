﻿using DMT;
using Harmony;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

public class TileEntityPatch : IHarmony
{
    public void Start()
    {
        var patchName = GetType().ToString();

        Debug.Log(" Loading Patch: " + patchName);
        var harmony = HarmonyInstance.Create(patchName);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    //[HarmonyPatch(typeof(TileEntity), "Instantiate")]
    //public class TileEntityPatch_Instantiate
    //{
    //    public static TileEntity Postfix(TileEntity __result, TileEntityType type, Chunk _chunk)
    //    {
    //        if(type == TileEntityType.EditableStorageBox)
    //        {
    //            return new TileEntityEditableStorageBox(_chunk);
    //        }

    //        return __result;
    //    }
    //}

    [HarmonyPatch(typeof(TileEntity), "Instantiate")]
    public class TileEntityPatch_Instantiate
    {
        public static bool Prefix(ref TileEntity __result, TileEntityType type, Chunk _chunk)
        {
            if(type == TileEntityType.EditableStorageBox)
            {
                __result = new TileEntityEditableStorageBox(_chunk);
                return false;
            }

            return true;
        }
    }
}
